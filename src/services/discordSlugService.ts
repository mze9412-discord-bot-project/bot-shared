// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-shared>.
//
// Bot-Shared is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { BotLogger, DiscordChannelService, DiscordMessageService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { CategoryChannel, Message, TextChannel, VoiceChannel } from "discord.js";
import { inject, injectable } from "inversify";
import { Logger } from "tslog";
import { DiscordSlugDatabaseProvider } from "../database/discordSlugDatabaseProvider";
import { DiscordSlug } from "../models/discordSlug";
import { DiscordSlugType } from "../models/enums/discordSlugType";
import { TYPES_BOTSHARED } from "../types";

@injectable()
export class DiscordSlugService {
    private _logger!: Logger;

    constructor(
        @inject(TYPES_BOTSHARED.DiscordSlugDatabaseProvider) private _discordSlugProvider : DiscordSlugDatabaseProvider,
        @inject(TYPES_BOTRUNNER.DiscordChannelService) private _discordChannelService : DiscordChannelService,
        @inject(TYPES_BOTRUNNER.DiscordMessageService) private _discordMessageService : DiscordMessageService,
        @inject(TYPES_BOTRUNNER.BotLogger) private _botLogger : BotLogger
    ) {
        this._logger = _botLogger.getChildLogger('DiscordSlugService');
    }
    
    public async getCategoryBySlug(guildId: string, slugName: string, categoryName: string, createIfNotExists: boolean, isPublic: boolean): Promise<CategoryChannel | undefined> {
        const slug = await this._discordSlugProvider.get(guildId, slugName);
        this._logger.debug(`GetCategoryBySlug: ${guildId}/${slugName}. CreateIfNotExists: ${createIfNotExists}. Slug found: ${slug != undefined}`);
        
        // if the slug is undefined, we need to create the category
        if (slug == undefined) {
            if (createIfNotExists) {
                this._logger.debug('Creating slug.');
                // create category, set to private if desired
                const category = await this._discordChannelService.createCategory(guildId, categoryName);
                if (!isPublic) await category.permissionOverwrites.edit(category.guild.roles.everyone, { VIEW_CHANNEL: false });

                // create slug
                const slug = new DiscordSlug();
                slug.guildId = guildId;
                slug.slugName = slugName;
                slug.slugType = DiscordSlugType.Category;
                slug.slugId = category.id;
                await this._discordSlugProvider.store(slug, true);
                
                return category;
            } else {
                this._logger.debug('Will not create non-existing category.');
            }
        } else {
            // get category by slug
            const category = await this._discordChannelService.getCategory(guildId, slug.slugId);
            if (category == undefined) {
                this._logger.debug(`Category not found by slug. Deleting slug. Creating new category: ${createIfNotExists}`);
                // delete slug
                await this._discordSlugProvider.delete(slug);

                // recursivly call self to create the category
                if (createIfNotExists) {
                    return await this.getCategoryBySlug(guildId, slugName, categoryName, createIfNotExists, isPublic);
                }
            }
            return category;
        }
    }
    
    public async getTextChannelBySlug(guildId: string, slugName: string, channelName: string, category: CategoryChannel, createIfNotExists: boolean, isPublic: boolean): Promise<TextChannel | undefined> {
        const slug = await this._discordSlugProvider.get(guildId, slugName);
        this._logger.debug(`TextChannelBySlug: ${guildId}/${slugName}. CreateIfNotExists: ${createIfNotExists}. Slug found: ${slug != undefined}`);

        // if the slug is undefined, we need to create the channel
        if (slug == undefined) {
                if (createIfNotExists) {
                // create channel, set to private if desired
                const channel = await this._discordChannelService.createTextChannel(guildId, channelName, category);
                if (!isPublic) await channel.permissionOverwrites.edit(channel.guild.roles.everyone, { VIEW_CHANNEL: false });

                // create slug
                const slug = new DiscordSlug();
                slug.guildId = guildId;
                slug.slugName = slugName;
                slug.slugType = DiscordSlugType.Channel;
                slug.slugId = channel.id;
                await this._discordSlugProvider.store(slug, true);
                
                return channel;
            } else {
                this._logger.debug('Will not create non-existing channel.');
            }
        } else {
            // get category by slug
            const channel = await this._discordChannelService.getTextChannel(guildId, slug.slugId);
            if (channel == undefined) {
                // delete slug
                await this._discordSlugProvider.delete(slug);
                this._logger.debug(`Channel not found by slug. Deleting slug. Creating new category: ${createIfNotExists}`);

                // recursivly call self to create the channel
                if (createIfNotExists) {
                    return await this.getTextChannelBySlug(guildId, slugName, channelName, category, createIfNotExists, isPublic);
                }
            }
            return channel;
        }
    }
    
    public async getVoiceChannelBySlug(guildId: string, slugName: string, channelName: string, category: CategoryChannel, createIfNotExists: boolean, isPublic: boolean): Promise<VoiceChannel | undefined> {
        const slug = await this._discordSlugProvider.get(guildId, slugName);
        this._logger.debug(`VoiceChannelBySlug: ${guildId}/${slugName}. CreateIfNotExists: ${createIfNotExists}. Slug found: ${slug != undefined}`);

        // if the slug is undefined, we need to create the channel
        if (slug == undefined) {
                if (createIfNotExists) {
                // create channel, set to private if desired
                const channel = await this._discordChannelService.createVoiceChannel(guildId, channelName, category);
                if (!isPublic) await channel.permissionOverwrites.edit(channel.guild.roles.everyone, { CONNECT: false });
                if (!isPublic) await channel.permissionOverwrites.edit(channel.guild.roles.everyone, { VIEW_CHANNEL: false });

                // create slug
                const slug = new DiscordSlug();
                slug.guildId = guildId;
                slug.slugName = slugName;
                slug.slugType = DiscordSlugType.Channel;
                slug.slugId = channel.id;
                await this._discordSlugProvider.store(slug, true);
                
                return channel;
            } else {
                this._logger.debug('Will not create non-existing channel.');
            }
        } else {
            // get category by slug
            const channel = await this._discordChannelService.getVoiceChannel(guildId, slug.slugId);
            if (channel == undefined) {
                // delete slug
                await this._discordSlugProvider.delete(slug);
                this._logger.debug(`Channel not found by slug. Deleting slug. Creating new category: ${createIfNotExists}`);

                // recursivly call self to create the category
                if (createIfNotExists) {
                    return await this.getVoiceChannelBySlug(guildId, slugName, channelName, category, createIfNotExists, isPublic);
                }
            }

            return channel;
        }
    }

    public async getMessageBySlug(guildId: string, slugName: string, channel: TextChannel): Promise<Message | undefined> {
        const slug = await this._discordSlugProvider.get(guildId, slugName);
        this._logger.debug(`MessageBySlug: ${guildId}/${slugName}. Slug found: ${slug != undefined}`);
        if (slug == undefined) {
            return;
        }
        await channel.messages.fetch();
        const message = await channel.messages.cache.get(slug.slugId);
        return message;
    }

    public async createMessageSlug(guildId: string, slugName: string, message: Message): Promise<void> {
        await this.createMessageSlugById(guildId, slugName, message.id);
    }

    public async createMessageSlugById(guildId: string, slugName: string, messageId: string): Promise<void> {
        this._logger.debug(`Creating MessageSlug ${guildId}/${slugName}/${messageId}`);
        const slug = new DiscordSlug();
        slug.guildId = guildId;
        slug.slugId = messageId;
        slug.slugName = slugName;
        slug.slugType = DiscordSlugType.Message;
        await this._discordSlugProvider.store(slug, true);
    }
}