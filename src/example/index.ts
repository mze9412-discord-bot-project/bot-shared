// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-shared>.
//
// Bot-Shared is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import "reflect-metadata";
import { BotLogger, BotRunner, BotRunnerConfig, ConfigService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { myContainer } from "./inversify.config";


const log = myContainer.get<BotLogger>(TYPES_BOTRUNNER.BotLogger).logger;

// register global callbacks
process.once("uncaughtException", err => {
    log.error(err);
});

// register global callbacks
process.once("uncaughtException", err => {
    log.error(err);
});

// log working dir
log.info(`Working Directory: ${process.cwd()}`);

// get config service and add config for botrunner
const configService = myContainer.get<ConfigService>(TYPES_BOTRUNNER.ConfigService);
configService.addConfig('botRunner', new BotRunnerConfig('', '', '', true));

// get bot
const bot = myContainer.get<BotRunner>(TYPES_BOTRUNNER.BotRunner);
bot.start();

// register to SIGINT and SIGTERM for graceful shutdown
process.once('SIGINT', () => {
    log.info('SIGINT: Stopping');
    bot.stop();
    log.info('SIGINT: DONE');
    process.exit(0);
});
process.once('SIGTERM', () => {
    log.info('SIGTERM: Stopping');
    bot.stop();
    log.info('SIGTERM: DONE');
    process.exit(0);
});