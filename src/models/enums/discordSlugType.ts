// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-shared>.
//
// Bot-Shared is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

export enum DiscordSlugType {
    Category,
    Channel,
    Message,
    Unknown
}